;;; speed-type-patterns is an elisp package for generating speed-type
;;; training buffers using most frequently used English words

(require 'cl-lib)
(require 'speed-type)

(defun speed-type-patterns--buffer-words (&optional begin end)
  (split-string
   (buffer-substring-no-properties (or begin (point-min))
                                   (or end (point-max)))))

(defun speed-type-patterns--read-words (file)
  (let ((buf (find-file-noselect file t t)))
    (unwind-protect
        (with-current-buffer buf
          (speed-type-patterns--buffer-words))
      (kill-buffer buf))))

;; (speed-type-patterns--read-words "1-1000.txt")

(defun speed-type-patterns--make-text (words)
  (with-temp-buffer
    (dotimes (j (max 10 (random 20)))
      (let ((word (nth (random (length words)) words)))
        (dotimes (i (max 2 (random 10)))
          (insert word " ")))
      (insert "\n"))
    (buffer-substring-no-properties (point-min) (point-max))))

(defvar speed-type-patterns--word-list-url
  "https://gist.githubusercontent.com/deekayen/4148741/raw/98d35708fa344717d8eee15d11987de6c8e26d7d/1-1000.txt")

(defun speed-type-patterns--read-words-url (url)
  (message "Fetching %s" url)
  (let ((buf (url-retrieve-synchronously url t t)))
    (unwind-protect
        (with-current-buffer buf
          (re-search-forward "^$")
          (next-line)
          (speed-type-patterns--buffer-words (point)))
      (kill-buffer buf))))

(defvar speed-type-patterns--words nil)
;; (setq speed-type-patterns--words nil)

(defun speed-type-patterns-words ()
  (or speed-type-patterns--words
      (setq speed-type-patterns--words
            (speed-type-patterns--read-words-url
             speed-type-patterns--word-list-url))))

(defun speed-type-patterns ()
  (interactive)
  (let ((text (speed-type-patterns--make-text
               (speed-type-patterns-words))))
    (speed-type--setup text "speed-type-pattern" "30 words")))

(provide 'speed-type-patterns)
